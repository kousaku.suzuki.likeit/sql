﻿create database practice DEFAULT CHARACTER SET utf8;
use practice;

create table item_category(
    category_id int PRIMARY KEY AUTO_INCREMENT,
    category_name varchar(256) NOT NULL
);
